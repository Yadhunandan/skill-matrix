$(document).ready(function () {


    $('[data-toggle="tooltip"]').tooltip();
    
    // Header JS Starts 

    $('#app-logo').on('click', function () {
        $(this).addClass('hide');
        $(this).siblings('.options').addClass('show');
    })

    $('header .options .profile-pic .outer-pp').on('click', function () {
        $(this).siblings('.logout-popup').fadeToggle();
        $(this).parents('.profile-pic').toggleClass('active');
    })

    $('header .options .profile-pic .logout-popup .cls-btn').on('click', function () {
        $(this).parents('.profile-pic').removeClass('active');
        $(this).parents('.logout-popup').fadeOut();
    })


    $(document).mouseup(function (e) {
        var container = $("header .options .profile-pic .logout-popup");
        if (!$('header .options .profile-pic img').is(e.target) && !$('header .options .profile-pic').is(e.target) && !container.is(e.target) && container.has(e.target).length === 0) {
            container.parents('.profile-pic').removeClass('active');
            container.fadeOut();
        }
    });

    // Header JS Starts 

    // common js code starts
    // let managerRatingBlockHeight = $(window).height()-$('.managers-landing .empl-list').height()-205;
    // $('.managers-landing .manager-rating-block').height(managerRatingBlockHeight);

    // common js code ends 

    /* Employee page js starts */

    $('.employee-landing .rating-block .catagory nav ul li').on('click', function () {
        let dataInfo = $(this).attr('data-info');
        $(this).parents('ul').find('li').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        })
        $(this).addClass('active');

        $('.employee-landing .rating-block .respective-rating-block').children('.indi-rating-block').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        });

        $('.employee-landing .rating-block .respective-rating-block').children('.indi-rating-block').each(function (index, item) {
            if (dataInfo == $(item).attr('data-info')) {
                $(item).addClass('active');
            }
        });
    })

    $('.employee-landing .profile-block .resume-sec .upload #resume').on('change', function () {
        $(this).siblings('.file-name').text($(this)[0].files[0].name);
        
    })

    function employeePageRating() {
        $('.employee-landing .rating-block .respective-rating-block .indi-rating-block .choose-box label ul li').on('click', function () {
            $(this).parents('ul').find('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })
            $(this).addClass('active');
            $(this).parents('label').siblings('input').val($(this).attr('data-num'));
            let tempWidth = 25 * ($(this).attr('data-num') - 1);
            $(this).parents('ul').siblings('.bar').animate({ width: tempWidth + '%' }, 300);
        })
    }

    employeePageRating();

    var toolArray = ["Angular", "HTML", "Java Script", "CSS", "Node JS"];

    $(".tool-select").select2({
        placeholder: "Select Tool"
    });


    $(".tech-select").select2({
        placeholder: "Select Tech"
    });


    $(".domain-select").select2({
        placeholder: "Select Domain"
    });


    $(".process-select").select2({
        placeholder: "Select Process"
    });

    $(".practice-select").select2({
        placeholder: "Select Practice"
    });

    $('.employee-landing .rating-block .respective-rating-block #tools-block .add-box').on('click', function () {
        $(this).before("<div class='choose-box new'><div class='delete'></div><select class='form-control tool-select' ><option value=''></option><input type='range' min='1' max='5' value='1' id='one'><label for='one'><div class='bar'></div><ul><li class='active' data-num='1'><div class='num-point'><p>1</p></div></li><li data-num='2'><div class='num-point'><p>2</p></div></li><li data-num='3'><div class='num-point'><p>3</p></div></li><li data-num='4'><div class='num-point'><p>4</p></div></li><li data-num='5'><div class='num-point'><p>5</p></div></li></ul></label></div>");
        employeePageRating();
        deleteBox();
        $.each(toolArray, function (key, value) {
            $('.employee-landing .rating-block .respective-rating-block #tools-block .add-box').prev('.choose-box').children('select')
                .append($('<option>', { value: key })
                    .text(value));
        });

        $('.employee-landing .rating-block .respective-rating-block #tools-block .add-box').prev('.choose-box').children('select').select2({
            placeholder: "Select Tool"
        })
    })


    $('.employee-landing .rating-block .respective-rating-block #tech-block .add-box').on('click', function () {
        $(this).before("<div class='choose-box new'><div class='delete'></div><select class='form-control tech-select' ><option value=''></option><input type='range' min='1' max='5' value='1' id='one'><label for='one'><div class='bar'></div><ul><li class='active' data-num='1'><div class='num-point'><p>1</p></div></li><li data-num='2'><div class='num-point'><p>2</p></div></li><li data-num='3'><div class='num-point'><p>3</p></div></li><li data-num='4'><div class='num-point'><p>4</p></div></li><li data-num='5'><div class='num-point'><p>5</p></div></li></ul></label></div>");
        employeePageRating();
        deleteBox();
        $.each(toolArray, function (key, value) {
            $('.employee-landing .rating-block .respective-rating-block #tech-block .add-box').prev('.choose-box').children('select')
                .append($('<option>', { value: key })
                    .text(value));
        });

        $('.employee-landing .rating-block .respective-rating-block #tech-block .add-box').prev('.choose-box').children('select').select2({
            placeholder: "Select Tech"
        })
    })


    $('.employee-landing .rating-block .respective-rating-block #domain-block .add-box').on('click', function () {
        $(this).before("<div class='choose-box new'><div class='delete'></div><select class='form-control domain-select' ><option value=''></option><input type='range' min='1' max='5' value='1' id='one'><label for='one'><div class='bar'></div><ul><li class='active' data-num='1'><div class='num-point'><p>1</p></div></li><li data-num='2'><div class='num-point'><p>2</p></div></li><li data-num='3'><div class='num-point'><p>3</p></div></li><li data-num='4'><div class='num-point'><p>4</p></div></li><li data-num='5'><div class='num-point'><p>5</p></div></li></ul></label></div>");
        employeePageRating();
        deleteBox();
        $.each(toolArray, function (key, value) {
            $('.employee-landing .rating-block .respective-rating-block #domain-block .add-box').prev('.choose-box').children('select')
                .append($('<option>', { value: key })
                    .text(value));
        });

        $('.employee-landing .rating-block .respective-rating-block #domain-block .add-box').prev('.choose-box').children('select').select2({
            placeholder: "Select Domain"
        })
    })


    $('.employee-landing .rating-block .respective-rating-block #process-block .add-box').on('click', function () {
        $(this).before("<div class='choose-box new'><div class='delete'></div><select class='form-control process-select' ><option value=''></option><input type='range' min='1' max='5' value='1' id='one'><label for='one'><div class='bar'></div><ul><li class='active' data-num='1'><div class='num-point'><p>1</p></div></li><li data-num='2'><div class='num-point'><p>2</p></div></li><li data-num='3'><div class='num-point'><p>3</p></div></li><li data-num='4'><div class='num-point'><p>4</p></div></li><li data-num='5'><div class='num-point'><p>5</p></div></li></ul></label></div>");
        employeePageRating();
        deleteBox();
        $.each(toolArray, function (key, value) {
            $('.employee-landing .rating-block .respective-rating-block #process-block .add-box').prev('.choose-box').children('select')
                .append($('<option>', { value: key })
                    .text(value));
        });

        $('.employee-landing .rating-block .respective-rating-block #process-block .add-box').prev('.choose-box').children('select').select2({
            placeholder: "Select Process"
        })
    })

    function deleteBox() {
        $('.employee-landing .rating-block .respective-rating-block .indi-rating-block .choose-box.new .delete').on('click', function () {
            $(this).parents('.choose-box.new').remove();
        })
    }

    /* Employee page js ends */


    /* Manager page js starts */

    function managerPageRating() {
        $('.managers-landing .manager-rating-block .table-section table td .manager-rating.rating label ul li').on('click', function () {
            $(this).parents('ul').find('li').each(function (index, item) {
                if ($(item).hasClass('active')) {
                    $(item).removeClass('active');
                }
            })
            $(this).addClass('active');
            $(this).parents('label').siblings('input').val($(this).attr('data-num'));
            let tempWidth = 25 * ($(this).attr('data-num') - 1);
            $(this).parents('ul').siblings('.bar').animate({ width: tempWidth + '%' }, 300);
        })
    }

    managerPageRating()


    $('.managers-landing .manager-rating-block .app-rej .reject-blk .inpt-brdr input').on('keyup', function () {
        if ($(this).val().length != 0) {
            $(this).parents('.inpt-brdr').removeClass('disbl');
            $(this).parents('.inpt-brdr').siblings('.reject-btn').removeClass('disbl');
        } else {
            $(this).parents('.inpt-brdr').addClass('disbl');
            $(this).parents('.inpt-brdr').siblings('.reject-btn').addClass('disbl');
        }
    })


    $('.managers-landing nav ul li').on('click', function () {
        let dataInfo = $(this).attr('data-info');
        $(this).parents('ul').find('li').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        })
        $(this).addClass('active');

        $('.managers-landing .manager-rating-block').children('.table-section').each(function (index, item) {
            if ($(item).hasClass('active')) {
                $(item).removeClass('active');
            }
        });

        $('.managers-landing .manager-rating-block').children('.table-section').each(function (index, item) {
            if (dataInfo == $(item).attr('data-info')) {
                $(item).addClass('active');
            }
        });
    })

    
    $(document).on('change', '.indi-rating-block .choose-box select', function () {
        let $this = $(this);
        let $val = $(this).find('option:selected').text();      
        $(this).parents('.indi-rating-block').find('select').not($(this)).each(function(index, item) {
           if($(item).find('option:selected').text() == $val) {
               errmsg('Please select diff one');
               $this.val('').trigger('change');
           }
        })
    })


    /* Manager page js ends */
})

function errmsg(msg){
    $("#basicExampleModal").modal()
}

document.onreadystatechange = function () {
    var state = document.readyState
    if (state == 'interactive') {
        //  document.getElementById('contents').style.visibility="hidden";
    } else if (state == 'complete') {
        setTimeout(function () {
            document.getElementById('preloader').style.display = "none";
        }, 1000);
    }
}

